/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function fetchUserDetails() {
		let fullName = prompt('What is your name?');
		let age = prompt('How old are you?');
		let location = prompt('Where do you live?');
		alert('Thank you for your input!');

		console.log('Hello ' + fullName);
		console.log('You are ' + age + ' years old');
		console.log('You live in ' + location);
	};

	fetchUserDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function printFavMusic() {
		console.log('1. Munimuni');
		console.log('2. Joji');
		console.log('3. Silent Sanctuary');
		console.log('4. Lany');
		console.log('5. Ben&Ben');
	};

	printFavMusic();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function printFavMovies() {
		console.log('1. Cast Away \n Rotten Tomatoes rating: 89%');
		console.log('2. Who am I: No system is safe \n Rotten Tomatoes rating: 73%');
		console.log('3. Snowden \n Rotten Tomatoes rating: 61%');
		console.log('4 The imitation game \n Rotten Tomatoes rating: 90%');
		console.log('5. The boy in the striped pajamas \n Rotten Tomatoes rating: 65%');
	};

	printFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);